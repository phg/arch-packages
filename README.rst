***********************************************************************
                               Packages
***********************************************************************

Meta-repo for my ArchLinux_ PKGBUILDs.
You should get a working package by executing::

    makepkg

in the root folder of each package.
If not, let me_ know!

The repository contains a ``Makefile`` in the toplevel directory by
means of which everything can be built in batch mode. Run::

    make info

for an overview of the defined build targets.

***********************************************************************
                                 About
***********************************************************************

Written and maintained (well, sort of) by Philipp Gesang. In an ideal
universe, the repo’s canonical location is on Bitbucket_.

Currently comprises development versions of these packages:

(#) LuaMD5_.            (``lua-md5-git``)
(#) `Selene Unicode`_   (``slnunicode-git``)
(#) `Luasec Prosody`_   (``luasec-prosody-git``),
(#) `Lua 5.3 work`_     (``lua53``),
(#) SWIG_               (``swig-git``)
(#) `Mutt-NNTP`_        (``mutt-nntp``)

.. _ArchLinux:        https://www.archlinux.org
.. _Bitbucket:        https://bitbucket.org/phg/packages
.. _LuaMD5:           https://aur.archlinux.org/packages/lua-md5-git/
.. _Selene Unicode:   https://aur.archlinux.org/packages/slnunicode-git
.. _SWIG:             https://aur.archlinux.org/packages/swig-git
.. _Luasec Prosody:   https://aur.archlinux.org/packages/luasec-prosody-git
.. _Lua 5.3 work:     https://aur.archlinux.org/packages/lua53/
.. _Mutt-NNTP:        https://aur.archlinux.org/packages/mutt-nntp/

.. _me:               mailto:phg42.2a@gmail.com

